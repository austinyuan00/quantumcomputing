// Austin Yuan and Patrick Kenney

import java.util.Random;

public class quantumKeyDistributer{

    public static void main(String[] args) {

        int n = 10000; // number of photons
        Random rand = new Random();

        // Alice --------------------------------------------

        // Alice generates the raw key.
        String keyAlice = new String();
        for (int i=0; i<n; i++) {       // Iterate over the number of photons.
            // Append a random character ('0' or '1') to the end.
            keyAlice += Integer.toString( rand.nextInt(2) );
        }

        // Alice chooses the encoding basis for each key bit.
        // This should be a string of '+'s and 'x's with '+'=H/V, 'x'=D/A.
        String basisAlice = new String();
        
        for (int i=0; i<n; i++) {
            basisAlice += Integer.toString( rand.nextInt(2) ); //appends a random basis for each key bit
        }


        // Alice selects a photon state according to the key and basis.
        // This should be a string of the characters 'H', 'V', 'D', 'A'.
        String photonAlice = new String();

        for(int i=0;i<n; i++) {
            String a = keyAlice.substring(i, i+1)+basisAlice.substring(i, i+1);
            switch(a){//switch statement to check basis and key
                case "00"://if in H/V basis and is 0
                    photonAlice += "H";
                    break;
                case "01"://if in H/V basis and is 1
                    photonAlice += "V";
                    break;
                case "10"://if in D/A basis and is 0
                    photonAlice += "D";
                    break;
                case "11"://if in D/A basis and is 1
                    photonAlice += "A";
                    break;
            }

        }
        double avgPhotonNumber = 5.0;

        // Alice prepares and sends each photon.
        // Use the methods of the Photon class to prepare each photon.
        Photon[] photonArray = new Photon[n];
        for (int i=0; i<n; i++) {
            photonArray[i] = new Photon();
        }

        for(int i=0; i<n; i++) {
            String a = photonAlice.substring(i, i+1);
            switch(a){//prepares each qubit according to qubitAlice
                case "H":
                    photonArray[i].prepareH(avgPhotonNumber);
                    break;
                case "V":
                    photonArray[i].prepareV(avgPhotonNumber);
                    break;
                case "D":
                    photonArray[i].prepareD(avgPhotonNumber);
                    break;
                case "A":
                    photonArray[i].prepareA(avgPhotonNumber);
                    break;
            }
        }

        // Eve   --------------------------------------------

        // Eve is allowed to do whatever she wants to the photonAlice array.
        // She cannot, however, have knowledge of Alice's or Bob's choice of bases,
        // nor Bob's measurement outcomes, until they are publicly announced.

        // Eve selects a subsample of photons from Alice to measure.
        // interceptIndex should be a string of n characters.
        // Use the convention '0'=ignored, '1'=intercepted
        String interceptIndex = "";
        // TODO: Put your code here.
        //for (int i = 0; i < n / 4; i++)
        //	interceptIndex += "0";
        for (int i = 0; i < n; i++)
        	interceptIndex += "1";

        // Eve chooses a basis to measure each intercepted photon.
        // basisEve should be a string of n characters.
        // Use the convention '+'=H/V, 'x'=D/A, ' '=not measured
        String basisEve = new String();

        for (int i=0; i<n; i++)
        	if (interceptIndex.charAt(i) == '1')
        		basisEve += ( rand.nextInt(2) == 0 ) ? '+' : 'x';
        	else
        		basisEve += " ";
        
        // Eve performs a measurement on each photon.
        // Use the methods of the Photon class to measure each photon.
        // outcomeEve should be a string of n characters.
        // Use the convention 'H','V','D','A', ' '=not measured
        String outcomeEve = new String();

        String temp;
        final double darkCounts = .3;
        for (int i=0; i<n; i++) {
        	if (basisEve.charAt(i) == '+')
        		temp = photonArray[i].measureHV(darkCounts);
        	else if (basisEve.charAt(i) == 'x')
        		temp = photonArray[i].measureDA(darkCounts);
        	else
        		temp = " ";
        	outcomeEve += (temp.charAt(0) == 'M' || temp.charAt(0) == 'N') ? ' ' : temp;
    	}

        // Eve resends photons to Bob.
        // Also handles the cases in which Eve gets an invalid measurement.

        final int avgPhotons = 5;
        for (int i=0; i<n; i++)
        	if (interceptIndex.charAt(i) == '1')
	        	switch (outcomeEve.charAt(i)) {
		        	case 'H':		photonArray[i].prepareH(avgPhotons);
		        					break;
		        	case 'V':		photonArray[i].prepareV(avgPhotons);
									break;
		        	case 'D':		photonArray[i].prepareD(avgPhotons);
									break;
		        	case 'A':		photonArray[i].prepareA(avgPhotons);
									break;
		        	case ' ':		photonArray[i].prepareH(0);
									break;
	        	}

        // Put any other nasty tricks here.


        // Bob   --------------------------------------------

        // Bob chooses a basis to measure each photon.
        // This should be a string of '+'s and 'x's with '+'=H/V, 'x'=D/A.
        String basisBob = new String();

        for (int i=0; i<n; i++) {
            basisBob += Integer.toString( rand.nextInt(2) );
        }
        // Bob performs a measurement on each photon.
        // Use the methods of the Photon class to measure each photon.
        // outcomeBob should be a string of n characters.
        // Use the convention 'H','V','D','A', ' '=not measured
        String outcomeBob = new String();

        double probDarkCount = 0.1;
        for(int i=0; i<n; i++) {
            String a = basisBob.substring(i, i+1);
            switch(a){
                case "0":
                    outcomeBob += photonArray[i].measureHV(probDarkCount);
                    break;
                case "1":
                    outcomeBob += photonArray[i].measureDA(probDarkCount);
                    break;
            }
        }
        // Bob infers the raw key.
        // keyBob should be a string of n characters.
        // Use the convention '0', '1', '-'=invalid measurement
        String keyBob = new String();

        for (int i=0; i<n; i++) {       // Iterate over the number of qubits
            String x = outcomeBob.substring(i,i+1);//checks the outcome
            switch(x) {//takes the outcome and infers the 0 or 1
                case "H":
                    keyBob += "0";
                    break;
                case "V":
                    keyBob += "1";
                    break;
                case "D":
                    keyBob += "0";
                    break;
                case "A":
                    keyBob += "1";
                    break;
                case "N":
                    keyBob += "-";
                    break;
                case "M":
                    keyBob += "-";
                    break;
            }
        }

        // -----------------------------------------------------------
        // Alice and Bob now publicly announce which bases they chose.
        // Bob also announces which of his measurements were invalid.
        // -----------------------------------------------------------


        // Alice and Bob extract their sifted keys.
        // siftedAlice and siftedBob should be strings of length n.
        // Use the convention '0', '1', ' '=removed
        String siftedAlice = new String();
        String siftedBob   = new String();

        for(int i=0; i<n; i++) {
            if(basisBob.charAt(i)==basisAlice.charAt(i)) {//checks if both bases are the same at each qubit
                if(keyBob.charAt(i)!='-') {
                    siftedAlice += keyAlice.charAt(i);//adds matching bases to the key
                    siftedBob += keyBob.charAt(i);
                    //same as above
                } else {
                    siftedAlice += " ";//adds matching bases to the key
                    siftedBob += " ";//same as above
                }
            } else if(basisBob.charAt(i)!=basisAlice.charAt(i)) {
                siftedAlice += " ";//adds a - to the sifted key
                siftedBob += " ";
            } else {
                siftedAlice += "-";//adds a - to the sifted key
                siftedBob += "-";
            }
        }
        
        // Alice and Bob use a portion of their sifted keys to estimate the quantum bit error rate (QBER).
        // sampleIndex should be a string of n characters.
        // Use the convention '0'=ignored, '1'=sampled
        // The QBER is the fraction of mismatches within the sampled portion.
        // For large samples, it should be close to the actual QBER,
        // which Alice and Bob, of course, do not know.
        String sampleIndex = "";
        int random = 0;
        double sampledBobQBER = 0.0;

        for(int i = 0; i < n; i++) {
            if(i <= siftedAlice.length()/4) {
                sampleIndex += "1" ;
            } else {
                random = rand.nextInt(3);
                if(random == 2) {
                  sampleIndex += "1";
                } else {
                    sampleIndex += "0";
                }
            }
        }


        double error = 0;
        for(int i = 0; i < n; i++){
            if(sampleIndex.charAt(i)=='1'){
                if(siftedAlice.charAt(i)!=siftedBob.charAt(i)){
                    error++;
                }
            }
        }
        sampledBobQBER = error/sampleIndex.length();
        // Alice and Bob remove the portion of their sifted keys that was sampled.
        // Since a portion of the sifted key was publicly revealed, it cannot be used.
        // secureAlice and secureBob should be strings of length n.
        // Use the convention '0', '1', ' '=removed
        String secureAlice = "";
        String secureBob   = "";

        for(int i = 0; i<n; i++) {
            if(sampleIndex.charAt(i)=='0'){
                secureAlice += siftedAlice.charAt(i);
                secureBob += siftedBob.charAt(i);
            } else {
                secureAlice += " ";
                secureBob += " ";
            }
        }

        // Alice and Bob make a hard determination whether the channel is secure.
        // If it looks like there's something fishy, better hit the kill switch!
        Boolean channelSecure = true; // default value, to be changed to false if Eve suspected

        if(sampledBobQBER > 0.1) {
            channelSecure = false;
        }


        // Eve ------------------------------------------------------------------

        // Eve infers the raw key.
        // keyEve should be a string of n characters.
        // Use the convention '0', '1', '-'=invalid measurement, ' '=not measured
        String keyEve = new String();

        for (int i=0; i<n; i++)
        	if (outcomeEve.charAt(i) == 'H' || outcomeEve.charAt(i) == 'D')
        		keyEve += "0";
        	else if (outcomeEve.charAt(i) == ' ')
        		keyEve += "-";
        	else
        		keyEve += "1";

        // Eve extracts her sifted key.
        // Knowing what Alice and Bob have publically revealed, Eve
        // now selects which portion of her sifted key to keep.
        // stolenEve should be strings of length n.
        // Use the '0', '1', ' '=removed
        String stolenEve = "";

        for (int i=0; i<n; i++)
        	if (basisAlice.charAt(i) == basisBob.charAt(i) && keyBob.charAt(i) != '-' && keyEve.charAt(i) != '-')
        		stolenEve += keyEve.charAt(i);
        	else
        		stolenEve += ' ';

        // ANALYSIS -------------------------------------------------------------

        // Below is a standard set of metrics to evaluate each protocol.

        // Compare Alice and Bob's sifted keys.
        int numMatchBob = 0;
        double actualBobQBER = 0;
        double secureKeyRateBob = 0;
        int secureKeyLengthBob = 0;
        for (int i=0; i<secureAlice.length(); i++) {
            if ( secureAlice.charAt(i) != ' ' ) {
                secureKeyLengthBob += 1;
                if ( siftedAlice.charAt(i) == siftedBob.charAt(i) ) {
                    numMatchBob += 1;
                }
            }
        }

        // Compute the actual quantum bit error rate for Bob.
        if (secureKeyLengthBob > 0) {
            actualBobQBER = 1 - (double) numMatchBob / secureKeyLengthBob;
        }
        else {
            actualBobQBER = Double.NaN;
        }
        // Compute the sifted key rate assuming each trial is 1 microsecond.
        if (secureKeyLengthBob > 0) {
            secureKeyRateBob = (1-actualBobQBER) * (double) secureKeyLengthBob / n * 1e6;
        }
        else {
            secureKeyRateBob = Double.NaN;
        }

        // Compare Alice and Eve's sifted keys.
        int numMatchEve = 0;
        double actualEveQBER = 0;
        double stolenKeyRateEve = 0;
        int stolenKeyLengthEve = 0;
        for (int i=0; i<stolenEve.length(); i++) {
            if ( stolenEve.charAt(i) != ' ' ) {
                stolenKeyLengthEve += 1;
                if ( secureAlice.charAt(i) == stolenEve.charAt(i) ) {
                    numMatchEve += 1;
                }
            }
        }
        // Compute the actual quantum bit error rate for Eve.
        if (stolenKeyLengthEve > 0) {
            actualEveQBER = 1 - (double) numMatchEve / stolenKeyLengthEve;
        }
        else {
            actualEveQBER = Double.NaN;
        }
        // Compute the sifted key rate assuming each trial is 1 microsecond.
        stolenKeyRateEve = (1-actualEveQBER) * (double) stolenKeyLengthEve / n * 1e6;


        // DISPLAY RESULTS ------------------------------------------------------

        System.out.println("");
        System.out.println("basisAlice  = " + basisAlice);
        System.out.println("basisBob    = " + basisBob);
        System.out.println("basisEve    = " + basisEve);
        System.out.println("");
        System.out.println("keyAlice    = " + keyAlice);
        System.out.println("keyBob      = " + keyBob);
        System.out.println("keyEve      = " + keyEve);
        System.out.println("");
        System.out.println("siftedAlice = " + siftedAlice);
        System.out.println("siftedBob   = " + siftedBob);
        System.out.println("");
        System.out.println("secureAlice = " + secureAlice);
        System.out.println("secureBob   = " + secureBob);
        System.out.println("stolenEve   = " + stolenEve);
        System.out.println("");
        if (!channelSecure) {
            secureKeyRateBob = 0;
            stolenKeyRateEve = 0;
            System.out.println("*********************************************");
            System.out.println("* ALERT! The quantum channel is not secure. *");
            System.out.println("*********************************************");
        }
        System.out.println("sampledBobQBER = " + Double.toString(sampledBobQBER));
        System.out.println("actualBobQBER  = " + Double.toString(actualBobQBER));
        System.out.println("actualEveQBER  = " + Double.toString(actualEveQBER));
        System.out.println("");
        System.out.println("secureKeyRateBob = " + Double.toString(secureKeyRateBob/1000) + " kbps");
        System.out.println("stolenKeyRateEve = " + Double.toString(stolenKeyRateEve/1000) + " kbps");

        // The goal is to maximize secureKeyRateBob and minimize stolenKeyRateEve.

    }

}
